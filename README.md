# Valhaim
**Table of Contents**

# My editing - right now only to 0.148.7v
# but gonna make it work for all versions

- Chat Commands : 
 - /iteminfo : 
example: /iteminfo Odin
result: Get all Odin Item, it shows as <ItemName>*<ItemHashID>

 - /additer
example: /additer <ItemName> <Amount_To_Drop> <Item_lvl> <Item_Speed_IfExist> <Item_armor_IfExist>
example: /additer Bow 1 10 0.1 99999 
example_information: Will spawn 1 Bow, with quality of lvl 10, with +10 Speed, With(out) armor +99999 (without because this item doesn't Have armor)
example: /additer CapeOdin 2 10 0.1 99999 
example_information: Will spawn 2 CapeOdin, with quality of lvl 10, with +10 Speed, With armor +99999 

 - /additem
 - /wind
 - /resetwind
example: as Same as Console "Spawn" Command
result :  as Same as Console "Spawn" Command

- DLC Require : 
Remove the Require, can Equip any DLC item





# Versions 

## Version: 0.153.2 - 12/05/2021
**GitUpload ->** 12/05/2021

### Path Note [Link](https://store.steampowered.com/news/app/892970/view/3071996229880181)
ood day, today we have a fine selection of fixes and tweaks for you guys to enjoy, including some much needed sparkling visual updates on a few of our older creatures. We also made some changes to the harpoon mechanics to make it less rubberband-y and more rope-y. Enjoy!


* Fixed smelter issue when a very long time has passed since visiting (10000 days)
* 30s Pre world save warning added
* Stone stair physics fix
* Inventory screen gamepad focus fix
* Large-creature visual overhaul (troll, boss-2 & boss-3)
* Troll ragdoll material fix
* Harpoon mechanics overhauled
* Engine updated (some stability improvements)
* Draugr archer visual fixes (1 & 2 star draugrs sometimes got the wrong material)
* Fixed issues when crafting station is destroyed while in use
* Creature group spawning adjusted to avoid spawning over total limit (in some cases)
* Credits updated
* Localization updates



## Version: 0.150.3 - 19/04/2021
**GitUpload ->** 12/05/2021

### Path Note [Link](https://store.steampowered.com/news/app/892970/view/5411613798265437192)

This was a long one! This patch has been taking extra long time to develop due to the new terrain-modification system. The new terrain modification system is made to reduce the number of network instances and make loading faster and smoother. Technically it’s a pretty big change but hopefully you should not notice that much of a difference except some minor behaviour changes to the hoe and pickaxe and of course much smoother loading of areas with a lot of terrain modifications. All terrain modifications after this patch (using the hoe, pickaxe or cultivator) will automatically use the new modification system. For existing areas where heavy terrain modifications have been made before this patch we have added a special console command “optterrain” that basically converts all the old terrain modifications in the nearby area to the new system. To use the “optterrain” console command, you first need to enable the ingame console by adding “-console” as a launch argument for the game.


0.150.3
* Swamp draugr spawner location fix to prevent draugrs from spawning inside stones
* Lox pet-sfx fix
* Torches in locations should no longer support constructions
* Dolmen location stone size fix
* New terrain modification system
* Terrain-modification priority changed (Terrain modifications in an area should load before buildings, only applies to the new terrain modification system)
* World loading tweaks (to fix issues with ships and buildings getting damaged while loading)
* Stop server list download when leaving the start menu (to decrease network bandwidth usage)
* Lowered the amount of stone required to Raise ground using the hoe


## Version: 0.148.7 - 29/03/2021
**GitUpload ->** 31/03/2021

### Path Note [Link](https://store.steampowered.com/news/app/892970/view/3025829894180343004)


Cute mini-tweak patch =)

* Localization updates
* Added separate walk-sneak snow footstep sfx
* Music update ( fixed some sound glitches )
* Credits updated ( Changed the look of the credits screen & added missing names )
* Hammer,Hoe & Cultivator timing & input tweaks ( Slightly lower use delay & queued button presses for a smoother experience...just for you )


## Version: 0.148.6 - 23/03/2021
**GitUpload ->** None....

Waahaa! This was a long one. Sorry for the delay, but we were waiting for a specific patch to the steam socket API, and it just went live today! (the fix in question is not listed in the steam changelog). I recommend you to make sure steam has updated to the "Mar 22 2021" version (Steam->Menu->Help->About Steam). Steam will of course automatically update itself to this version. Lots of big and small changes in this patch, some gameplay tweaks and some bug fixes.

NOTE: Don't forget to update your server as well.

* Campfire,Bonfire & hearth take damage when dealing damage
* Reinforced chest inventory space increased to 6x4
* All boss drops can now float on water
* Sunken crypt entrance tweaked (to stop tombstones from getting stuck)
* Fixed rotation of Wood tower shield on item stands
* Deathsquito & Drake trophy drop rate increased
* 1 & 2 Star creature HP fix
* Night-spawning wolves should be easier to tame now (should stop trying to run away & despawn after starting to tame)
* Harpoon does not work on bosses anymore
* Ingame console disabled by default (add launch argument "-console" to enable)
* The console command for enabling developer/debug commands has been changed to “devcommands” from “imacheater” and a warning message has been added.
* Improved enemy projectile reaction system
* Battle axe tweaks (hits multiple enemies easier)
* Player knockback force is affected by equipment speed modifiers (IE heavy gear will reduce the knockback from enemies)
* Blackforest stone tower tweaks
* Ward system fixes (You can no longer place a new ward where an enemy ward overlaps)
* Comfort calculation fixed
* "Failed to connect" error message fixed
* Serpent trophy stack fix
* Missing Moder spawn location in some worlds fixed (NOTE: For existing worlds "genloc" command needs to be run manually in a local game with dev commands enabled to generate new locations, this is only needed if your specific world has this issue, this is not very common)
* Megingjord item-collider fix
* Added a slight use-delay on Hammer, Hoe & Cultivator
* Hammer remove auto-repeat added
* Better network bandwidth handling (should work better on low bandwidth connections & also use higher data rate if possible)
* Dolmen location fixes (Stop top stone from falling for no reason)
* Fixed removing item from item-stand not always syncing item stats
* Server list refresh button can be pressed before the entire list has been downloaded
* Better bad connection detection
* Fixed issue causing server to send more data the longer a client was connected
* Localization updates
MORE ABOUT THIS GAME



## Version: 0.147.3 - 24/02/2021
**GitUpload ->** 31/03/2021


### Path Note [Link](https://steamcommunity.com/games/892970/announcements/detail/3058478454632856787)
Good day Valheimers!
Todays patch has some huge changes to dedicated servers. Dedicated servers now always use direct connections instead of using Steam Datagram Relay (SDR). This should result in much lower latency for most players. Private dedicated servers are enabled by adding "-public 0" to the server command line (See server manual PDF). You can only connect to private dedicated servers using the "Join IP"-button. I really hope this patch solves a lot of connection issues introduced in last weeks patches ( due to switching socket backend ). In addition to these dedicated-server fixes valve are also working on fixes to the SDR system (used when connecting to non-dedicated servers) and i believe there is already a beta-steam-client available that fixes some of these issues. Thanks everyone!!


* Localization updates
* Made Haldor head-turn smoother
* Object network interpolation is skipped if object was far away, solved issue with network players flying through the air when entering dungeons & exiting portals etc
* Added -public 1/0 flag to dedicated server again, Allows players to host local lan only servers
* Join IP-button updated to allow for lan-connections (dedicated servers only) & added DNS support
* Dedicated servers use directIP connection instead of SDR, solves issues with slow steam relays in some areas of the world
* Bonemass puke-effect network fix
* Updated Dedicated-server PDF manual
* Prevent pickup items when entering portals
* Lowered wolf procreation slightly
* Lowered chance of boss trophy talking
